#!/bin/sh

if [ -z "$KUBECONFIG" ]; then
  source get-k8s-creds
fi

set -e

NODEPORT="$(kubectl get service nginx-ingress-controller -o "jsonpath={.spec.ports[?(@.port==80)].nodePort}")"

# grafana / prometheus
helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
echo ">> Installing Prometheus Operator (this may take a while)..."

helm upgrade --install prometheus-operator coreos/prometheus-operator
helm upgrade --install \
  --set alertmanager.ingress.hosts={alertmanager.${UCP_HOSTNAME}} \
  --set prometheus.ingress.hosts={prometheus.${UCP_HOSTNAME}} \
  --set grafana.ingress.hosts={grafana.${UCP_HOSTNAME}} \
  --values /helm/prometheus-values/values.yaml \
  kube-prometheus coreos/kube-prometheus 

# jaeger
helm upgrade --install \
  --set query.ingress.hosts={jaeger.${UCP_HOSTNAME}} \
  --values /helm/jaeger/values.yaml \
  jaeger /helm/jaeger

echo "----------------------------------------------------------"
echo "   ingress url(s):"
echo "      alertmanager:  http://alertmanager.${UCP_HOSTNAME}:${NODEPORT}"
echo "      grafana:       http://grafana.${UCP_HOSTNAME}:${NODEPORT}"
echo "      jaeger:        http://jaeger.${UCP_HOSTNAME}:${NODEPORT}"
echo "      prometheus:    http://prometheus.${UCP_HOSTNAME}:${NODEPORT}"

