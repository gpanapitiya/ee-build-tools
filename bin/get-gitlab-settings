#!/bin/sh

if [ -z "$KUBECONFIG" ]; then
  source get-k8s-creds
fi

if [ -z "$1" ]; then
  echo "Usage: $0 USERNAME" >&2
  exit 1
fi

set -e

function _kubectl() {
  kubectl $@ $kubectl_options
}

DEPLOY_USER="$1"
shift
kubectl_options="$@"

NAMESPACE=${NAMESPACE:-spc}
CONTEXT="$(kubectl config current-context)"
CLUSTER="$(kubectl config view -o "jsonpath={.contexts[?(@.name==\"$CONTEXT\")].context.cluster}")"
SERVER="$(kubectl config view -o "jsonpath={.clusters[?(@.name==\"$CLUSTER\")].cluster.server}")"
NODEPORT="$(kubectl get service nginx-ingress-controller -o "jsonpath={.spec.ports[?(@.port==80)].nodePort}")"

SECRET="$(_kubectl get serviceaccount "$DEPLOY_USER" -o 'jsonpath={.secrets[0].name}' --namespace=$NAMESPACE)"
if [ -z "$SECRET" ]; then
  echo "Service account $DEPLOY_USER not found" >&2
  exit 1
fi
CA_CRT_DATA="$(_kubectl get secret "$SECRET" -o "jsonpath={.data.ca\.crt}" --namespace=$NAMESPACE | openssl enc -d -base64 -A)"
TOKEN="$(_kubectl get secret "$SECRET" -o "jsonpath={.data.token}" --namespace=$NAMESPACE | openssl enc -d -base64 -A)"

echo "Configure GitLab with the following settings:"
echo
echo "  CA Certificate:"
echo
echo "$CA_CRT_DATA"
echo
echo "  K8S API:           $SERVER"
echo "  Token:             $TOKEN"
echo "  App Namespace:     $NAMESPACE"
echo
echo "Environment Variables:"
echo
echo "  UCP_HOSTNAME:      $UCP_HOSTNAME"
echo
echo "The Spring PetClinic app will be available at these URLs once deployed in Gitlab:"
echo
echo "  SPC URL:    http://spc.$UCP_HOSTNAME:$NODEPORT"
echo "  Admin URL:  http://admin.spc.$UCP_HOSTNAME:$NODEPORT"

